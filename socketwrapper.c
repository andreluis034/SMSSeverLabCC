#include "socketwrapper.h"

/**
 * Inicializar a ouvir socket na porta dada 
 * 
 * @param   {int} Porta em que ira inicializar o servidor
 * @return  {int} Socket Criado
 */
int initSocket(int port)
{
    struct sockaddr_in dest;

	/* Chamar socket()
	**	AF_INET - support IPv4
	**	SOCK_STREAM - Enviar e receber dados
	*/
    socketf = socket(AF_INET, SOCK_STREAM, 0);
    if ( socket < 0 )
    {
        perror("Error ao abrir socket");
        return -1;
    }
    int enable = 1;
    setsockopt(socketf, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)); 

    //Inicializar a struct stockaddr_in(address/port)
    memset(&dest, 0, sizeof(dest));
    dest.sin_family = AF_INET;
    dest.sin_addr.s_addr = INADDR_ANY; //Bind socket a todos os enderecos/interfaces
    dest.sin_port = htons(port);

    /* inicializar estrutura do socket */
    memset(&dest, 0, sizeof(dest)); //escrever zeros na memoria para remover o lixo
    dest.sin_family = AF_INET; //TIPO(IPv4)
    dest.sin_addr.s_addr = INADDR_ANY; //Bind socket a todos os enderecos/interfaces
    dest.sin_port = htons(port);

    if (bind(socketf, (struct sockaddr *) &dest, sizeof(dest)) < 0)
    {
        perror("ERRO:(porta já a ser usada?)");
        exit(1);
    }
    //Ouvir todas as conecoes com destino para socket com uma queue(fila) máxima de 5
    listen(socketf, 5);

	return socketf; //Criamos o socket com sucesso
}

/**
 * Le os conteudos no socket para um buffer dado(Pode ser realocado)
 * 
 * @param   {int}       Socket que vai ser utilizado para ler a mensagem
 * @param   {void**}    Apontador para o apontador de o buffer, este pode ser realocado caso não 
 *                          seja grande suficiente
 * @param   {int}       Tamanho inicial do buffer
 * @return  {int}       Tamanho lido
 */
int readSocket(int socket, void** buffer, int length)
{
    //mais legivel do que trablhar com imensos void **
    Message* m = *((Message **)buffer);
    int tempLength;
    int status = read(socket, &m->Length, sizeof(int)); //Lemos os quatro primeiros bytes que são o Message.Length
    if (status >= 0)
    {
        if (m->Length > length)
        {
            //Nada nos garante que o conteudo em *buffer continuem intacto depois do realloc
            tempLength = m->Length;
            void* tempPointer = realloc(*buffer, tempLength);
            //caso nao consigo re alocar a memoria necessária(out of memory?) nao perder o apontador
            if (tempPointer != NULL)
            {
                *buffer = tempPointer;
                length = tempLength;
            }
        }
        //Queremos ler a informacao a asseguir à Length por isso o nosso endereço onde vamos
        //escrever passa a ser buffer + sizeof(int) 
        status = read(socket, (void*)(*buffer + 4), length-sizeof(int));
    }

    if (status < 0)
    {
        #ifdef __DEBUG__
        perror("Erro ao ler socket");
        #endif
    }
    return status;
}

/**
 * Escreve o buffer dado no socket
 * 
 * @param   {int}       Socket que vai ser utilizado para enviar o buffer
 * @param   {void*}     Buffer que vai se escrito  no socket
 * @param   {int}       Tamanho do buffer
 * @return
 */
int writeSocket(int socket, void* buffer, int size)
{
	int status = write(socket, buffer, size);
	if (status < 0)
	{
		perror("Error ao escrever no socket");
	}
	return status;
}

/**
 * Fechar a ligacao
 */
void closeSocket()
{
	close(socketf);
}
