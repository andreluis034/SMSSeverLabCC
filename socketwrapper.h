#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/in.h>
#include <string.h>
#include "messagehelper.h"

int socketf;

int initSocket(int);
//Read/WriteSocket fazem mais sentido no cliente
int readSocket(int, void**, int);
//int writeSocket(char*,int); //Nao existe/funciona em C
int writeSocket(int, void*, int);
//void closeSocket(int);
void closeSocket();