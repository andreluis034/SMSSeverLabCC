#pragma once
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "defs.h"

typedef struct 
{
	char name[MAX_USER_LENGTH];
	char password[MAX_USER_LENGTH]; //Caso necessario podiamos implementar um algoritmo de hash
} User;

typedef struct
{
	User* user;
	int slot;
	int socket; //Socket/Ligacao a ser usada para falar com utilizador
	pthread_t thread; //Para controlar na said do servidor
} OnlineUser;
//nao declarar variaveis nos headers
//http://stackoverflow.com/questions/17764661/multiple-definition-of-linker-error
extern User 		emptyUser;
extern OnlineUser	emptyOnlineUser;
extern User 		Users[MAX_USERS];
extern OnlineUser 	OnlineUsers[MAX_ONLINE];

int UserExists(char* username);
int CheckUser(char* username, char* password);
int findAvailableOnlineSlot(); //Encontrar a primeira posicao livre no array OnlineUsers
int getOnlineUserIdFromName(char* username);
int getOnlineUserCount();
void unsetOnlineUser(int id);
int LoadUsers();
int AddUser(char* username);
