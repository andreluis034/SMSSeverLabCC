#include <stdio.h>
#include <stdlib.h>
#include <pthread.h> //Threads para usar nas nossas funcoes e correr asincronamente
#include <netdb.h>
#include <netinet/in.h>
//http://stackoverflow.com/questions/15635803/segmentation-fault-for-inet-ntoa
#include <arpa/inet.h> //Para utilizar char* inet_ntoa(struct in_addr)
#include <string.h>
#include <signal.h>
#include "user.h"
#include "socketwrapper.h"
#include "messagehelper.h"
#include "defs.h"

//Porta utilizada pelo socket
#define DEFAULT_PORT    1337
int PORT = DEFAULT_PORT;
int socketf;
int NSAMODE = 0;
int exitServer = 0;
void* OnlineUsersThread(void*);
void* LoginUsersThread();
void ShutdownServer();

/**
 * Caso CTRL+C ou SIGINT seja enviado para o programa fechar todos os sockets seguramente
 */
void emergencyExit(int signum)
{
	printf("CTRL+C detectado a fechar servidor seguramente...\n");
	ShutdownServer();
	exit(0);
}

/**
 * Imprime o help(./file -h)
 * @param {char*}			Só para formatacao
 */	
void PrintHelp(char* fileName)
{
	printf("Utilizacao: %s [OPÇÕES]...\n", fileName);
	puts("Simples servidor de mensagens utilizando sockets");
	puts("	adduser user 		Adiciona o user à base da dados, o servidor não é executado");
	puts("	-p port 			Porta utilizada pelo servidor. Default: 1337");
	puts("	-NSA 				Mostra as mensagens enviadas pelos utilizadores");
	puts("	-h, --help 			Mostra esta mensagem\n");
	printf("Para adicionar um user tera de correr %s adduser user sem quaisqueres outros argumentos\n\n", fileName);
	puts("Exemplos:\n");
	printf("%s adduser manel	Verifica se o utilizador manel existe e se não pede a password\n", fileName);
	printf("%s 			Corre o servidor na porta 1337 e nao mostra as mensagens enviadas entre os utilizadores\n", fileName);
}

/**
 * Lê os argumentos todos e faz as suas funções 
 * @param  {int} 			Numero de argumentos
 * @param  {char*} 			Array com os argumentos
 * @return {int} 			1 - Imprimiu o help e o programa vai acabar
 *                  		0 - O programa vai continuar...
 */
int ParseArguments(int argc, char *argv[])
{
	int i;
	for (i = 1; i < argc; ++i)
	{
		if (strcmp(argv[i], "-p") == 0 && i+1 < argc)
		{
			PORT = atoi(argv[i+1]);
			if (PORT == 0)
				PORT = DEFAULT_PORT;
		}
		else if (strcmp(argv[i], "-NSA") == 0)
		{
			NSAMODE = 1;
		}
		else if (strcmp(argv[i], "--help") == 0 || strcmp(argv[i], "-h") == 0)
		{
			PrintHelp(argv[0]);
			return 1;
		}
	}

	return 0;
}

/**
 * Desliga o servidor, desconectando todos os utilizadores primeiro
 */
void ShutdownServer()
{
	int i;
	Message* haltMessage;

	for (i = 0; i < MAX_ONLINE; ++i)
	{
		if (memcmp(&OnlineUsers[i], &emptyOnlineUser,
			sizeof(OnlineUser)) == 0)
			continue;

		haltMessage = createMessage(HALT, "\0", "\0", "\0");
		writeSocket(OnlineUsers[i].socket, haltMessage, haltMessage->Length);
		free(haltMessage);
		close(OnlineUsers[i].socket);
		pthread_join(OnlineUsers[i].thread, NULL);
		usleep(10000);
	}
	close(socketf);	
}

/**
 * Thread utilizado para fazer login as users
 */
void* LoginUsersThread()
{
	//http://www.gta.ufrj.br/ensino/eel878/sockets/sockaddr_inman.html
	struct sockaddr_in clientAddr;
	int clientLength = sizeof(clientAddr);
	socketf = initSocket(PORT);
	char* password;
	int slot, ID;
	int tempSlot;

	if (socketf <0)
		return NULL;

	Message* sndMessage;
    Message* rcvMessage = malloc(sizeof(Message));

	printf("A aceitar ligações em: 0.0.0.0:%d\n", PORT);
    puts("Servidor iniciado.");
    puts("Em modo de espera de mensagens");
    //Os logins serao feitos "um a um"
    while(1) 
    {
		/*Aceitamos a coneccao e posteriormente criamos outro processo
		o utilizador para que desta forma podermos falar com vários clientes*/
		int newSocket = accept(socketf, 
		    (struct sockaddr *) &clientAddr, 
		   	(socklen_t *)&clientLength);
		int status;
		if (newSocket < 0) 
		{
		    perror("Erro ao aceitar ligacao:");
		    continue;
		}

		//http://www.gta.ufrj.br/ensino/eel878/sockets/sockaddr_inman.html
		//http://linux.die.net/man/3/inet_aton
		printf("Nova Ligacao: %s:%d\n", 
			inet_ntoa(clientAddr.sin_addr), 
			(int)clientAddr.sin_port);
		
		memset(rcvMessage, 0, sizeof(Message)); //Colocar mensagem a zeros para a receber
		status = readSocket(newSocket, (void**)&rcvMessage, sizeof(Message));
		if (status < 0
			|| rcvMessage->Type != LOGIN1 
			|| strlen(rcvMessage->Content) < 1 
			|| strstr(rcvMessage->Content, " ") == NULL)
		{
			printf("Ligacao Invalida\n");
			close(newSocket);
			continue;
		}

		//A mensagem inicial tem o formato "username password"
		strtok_r(rcvMessage->Content, " ", &password);
		ID = CheckUser(rcvMessage->Content, password);

		sndMessage = createMessage(ID < 0 ? LOGIN2 : OK, "\0", "\0", "\0");
		writeSocket(newSocket, sndMessage, sndMessage->Length);
		free(sndMessage);

		if (ID >= 0) // User existe
		{
			slot = findAvailableOnlineSlot();
			tempSlot = getOnlineUserIdFromName(Users[ID].name);
			if (tempSlot != -1) //O utilizador encontra-se online e temos de forçar logout
			{
				printf("* O Utilizador %s iniciou sessão novamente com a sessão anterior ainda válida\n", 
					Users[ID].name);
				printf("* A fechar ligação anterior\n");
				sndMessage = createMessage(LOGOUT, "\0", "\0", "Sessão iniciada noutro local");
				writeSocket(OnlineUsers[tempSlot].socket, sndMessage, sndMessage->Length);
				free(sndMessage);
				close(OnlineUsers[tempSlot].socket);
				pthread_join(OnlineUsers[tempSlot].thread, NULL);
				slot = tempSlot;
			}
			OnlineUsers[slot].user = &Users[ID];
			OnlineUsers[slot].slot = slot;
			OnlineUsers[slot].socket = newSocket;
			#ifdef __DEBUG__
			printf("Assigned %s ID: %d\n", Users[ID].name, slot);
			#endif
			//Criar o thread para o user
			pthread_create(&OnlineUsers[slot].thread, NULL, 
				&OnlineUsersThread, (void*)&OnlineUsers[slot]);

			printf("%s* Utilizador %s iniciou a sua sessão com sucesso%s\n", 
				KGRN, Users[ID].name, KNRM);
			continue;
		}

		close(newSocket);
    }
    free(rcvMessage);
	close(socketf);
	return NULL;
}

/**
 * Cada utilizador vai ter um Thread para receber e enviar mensagens
 * @param {OnlineUser*} 			Utilizador ue vai enviar mensagens
 */	
void* OnlineUsersThread(void* onlineUser1)
{
	OnlineUser* onlineUser = (OnlineUser*) onlineUser1;
    Message* rcvMessage = malloc(sizeof(Message)); //Receber mensagens
    Message* sndMessage;	//Enviar mensagens
    int status;
	int slot = onlineUser->slot;
	int logout = 0;
	while(1)
	{
		memset(rcvMessage, 0, sizeof(Message));
		status = readSocket(onlineUser->socket, (void**)&rcvMessage, sizeof(Message));
		if (status < 0)
		{
			if (exitServer == 0)
			{
				fprintf(stderr, "Conecção com %d:%s perdida\n", 
				slot, onlineUser->user->name);
			}

			//fechar a ligacao
			break;
		}
		switch(rcvMessage->Type)
		{
			case SENDMSG: //Vamos fazer forward a mensagem para outro utilizador
				sendMessage(rcvMessage, onlineUser->user->name);
				break;
			case LOGOUT: //Pediu logout
				logout = 1;
				break;
			case INFO:  //Pediu informacao
				if(strcmp(rcvMessage->Content,"/onlineusers")==0)
				{
					int contador;
					char users[MAX_ONLINE*MAX_USER_LENGTH];
					int contaposicao = 0;
					for(contador=0;contador<MAX_ONLINE;contador++)
					{
						if (OnlineUsers[contador].user == NULL)
						{
							continue;
						}
							
						strcpy(&users[contaposicao],OnlineUsers[contador].user->name);
						contaposicao += strlen(OnlineUsers[contador].user->name);
						users[contaposicao] = '\n'; contaposicao++;
							
					}
					users[contaposicao] = '\0';
					sndMessage = createMessage(INFO,"\0","\0",users);
					writeSocket(OnlineUsers[slot].socket, sndMessage, sndMessage->Length);
					free(sndMessage);
				}
				break;
			default: //Mensagem não suportada
				if (status == 0 && rcvMessage->Length == 0 && rcvMessage->Type == 0)
				{
					logout = 1;
					break;
				}
				#ifdef __DEBUG__
				fprintf(stderr, 
					"Tipo de mensagem não suportada recebida de: %s\n", 
					onlineUser->user->name);
				#endif

				break;
		}
		if (logout)
		{
			sndMessage = createMessage(LOGOUT, "\0", "\0", "Requested");
			writeSocket(OnlineUsers[slot].socket, sndMessage, sndMessage->Length);
			free(sndMessage);
			break;
		}
	}

	//fechar ligação/socket, e tirar o user da lista de utilizadores online
	close(onlineUser->socket);

	#ifdef __DEBUG__
		printf("%s# %d:%s fez logout%s\n", KGRN, slot, onlineUser->user->name, KNRM);
		printf("Unsetting %d:%s, slot %d\n", slot, onlineUser->user->name, slot);
		unsetOnlineUser(slot);
		printf("Unsetted %d\n", slot);
	#else
		printf("%s# %s fez logout\n%s", KGRN, onlineUser->user->name, KNRM);
		unsetOnlineUser(slot);
	#endif
		
	free(rcvMessage);

	return NULL;
}

int main(int argc, char *argv[])
{

	if (ParseArguments(argc, argv))
		return 0;

	#ifdef __DEBUG__
	strcpy(Users[0].name, 		"admin");
	strcpy(Users[0].password, 	"admin");
	#endif
	LoadUsers();
	if (argc == 3 && strcmp(argv[1], "adduser") == 0)
	{
		AddUser(argv[2]);
		return 0;
	}

	pthread_t tid; //ID do thread
	pthread_create(&tid, NULL, &LoginUsersThread, (NULL));

	char option;

   	signal(SIGINT, emergencyExit);
	while(1)
	{
		option = getchar();getchar(); //Consumir \n
		if (option == 'Q' || option == 'q')
		{
			
	        printf("%s!!!!!%s\n", KRED, KNRM);
	        printf("Tem a certeza que pretende terminar o servidor?\n");
	        int onlineCount = getOnlineUserCount();
	        printf("Neste momento %s %d %s online\n", (onlineCount == 1 ? "está" : "estão"),
	        	onlineCount, (onlineCount == 1 ? "utilizador" : "utilizadores"));
	        printf("Confirme por favor (s/n) ?"); option = getchar();getchar();//\n
			if (option == 's' || option == 'S')
			{
				exitServer = 1;
	        	printf("!!!!! A enviar mensagem de terminação a todos os clientes\n");
				ShutdownServer();
	       		printf("!!!!! Servidor terminado !!!!!\n");
				break;
			}
	        printf("%s!!!!!%s\n", KRED, KNRM);
		}
	}
	return 0;
}
