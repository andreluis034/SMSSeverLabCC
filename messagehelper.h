#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "defs.h"
#include "user.h"
#include "socketwrapper.h"
/*
** 	LOGIN1 	- Enviar username(Apensa enviada pelos clientes)
**	LOGIN2  - Envio e validacao do password
**	OK 		- Autenticado(Apenas enviado pelo servidor)
**	NOTOK	- Não Autenticado(Apenas enviado pelo servidor)
**	RECVMSG - Mensagem recebida(Apensa enviada pelo servidor)
**	SENDMSG - Mensagem recebida(Apensa enviada pelos clientes)
**	WARN	- Caso nao seja possivel enviar a mensagem
**	HALT	- Desligar servidor (Apenas enviado pelo servidor)
**	INFO	- Enviar ou pedir informacao que nao se encaixa em nenhum dos contextos acima^
*/
typedef enum 
{
	LOGIN1, LOGIN2, LOGOUT, OK, NOTOK, RECVMSG, SENDMSG, WARN, HALT, INFO
} MessageType;

//estrutura do pacote, idealmente caso o tamanho nao fosse divisil por quatro 
//usar o atributo packed contudo isto pode tras alguns problemas nalgumas arquiteturas?
//http://stackoverflow.com/questions/4306186/structure-padding-and-packing
typedef struct 
{
										//Posição na memória
	int Length;							//0x00 Tamanho total do pacote
	MessageType Type; 					//0x04
	char Sender[MAX_USER_LENGTH];		//0x08
	char Destination[MAX_USER_LENGTH];	//0x08 + MAX_USER_LENGTH(default 20)
	//Na verdade o tamanho vai ser maior que por causa do "hack" com o malloc
	char Content[1];	   				//0x08 + MAX_USER_LENGTH*2
} Message;

int NSAMODE;

int actualMessageSize(Message*);
Message* createMessage(MessageType, char*, char*, char*);
int sendMessage(Message*, char*);
void printMessage(Message*);