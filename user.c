#include "user.h"

User 		Users[MAX_USERS] = {{{0}}};
OnlineUser 	OnlineUsers[MAX_ONLINE] = {{0}};
//Arrays especificos para verificar se o slot está vazio
User 		emptyUser ={{0}};
OnlineUser	emptyOnlineUser = {0};

/**
 * Dado um username, retorna caso exista o seu id caso contrário -1.
 * 
 * @param {char*}		Username a procurar
 * @return {int}		id to utilizador
 */
int UserExists(char* username)
{
	int i;
	for (i = 0; i < MAX_USERS; ++i)
	{
		if (strcmp(username, Users[i].name) == 0)
		{
			return i;
		}
	}
	return -1;
}

/**
 * Dado uma combinação username/password verifica se este existe na "base de dados"
 * 
 * @param {char*}		Username a verificar
 * @param {char*}		Password a verificar
 * @return {int}		Retorna o id do utilizador se a combinação username/password estiver correta			
 */
int CheckUser(char* username, char* password)
{
	int i = UserExists(username);
	if (i < 0)
	{
		return -1;
	}
	return strcmp(password, Users[i].password) == 0 ? i : -1;
}


/**
 * Encontra o primeiro slot nos utilizadores online para alocar o novo utilizador
 * 
 * @return {int} slot para guardar o utilizador online
 */
int findAvailableOnlineSlot()
{
	//int maxLoop = sizeof(OnlineUsers) / sizeof(OnlineUser);
	int i;
	for (i = 0; i < MAX_ONLINE; ++i)
	{
		if (memcmp(&OnlineUsers[i], &emptyOnlineUser,
			sizeof(OnlineUser)) == 0)
		{
			return i;
		}
	}
	return -1;
}


/**
 * Dado um username retorna o idOnline do utilizador
 * 
 * @param {char*}		Username
 * @return {int}
 */
int getOnlineUserIdFromName(char* username)
{
	int maxLoop = sizeof(OnlineUsers) / sizeof(OnlineUser);
	int i;
	for (i = 0; i < maxLoop; ++i)
	{
		//para evitar que o strcmp aceda a um apontador não existente e crashe o servidor
		if (OnlineUsers[i].user == NULL)
		{
			continue;
		}
		if (strcmp(username, OnlineUsers[i].user->name) == 0)
		{
			return i;
		}
	}
	return -1;
}

/**
 * Retorna o numero de utilizadores online
 * @return {int} 		Numero de utilizadores online
 */
int getOnlineUserCount()
{
	int i, j = 0;
	for (i = 0; i < MAX_ONLINE; ++i)
	{
		if (memcmp(&OnlineUsers[i], &emptyOnlineUser,
			sizeof(OnlineUser)) != 0)
		{
			j++;
		}
	}
	return j;
}

/**
 * Remove o utilizador da lista de utilizadores online
 * 
 * @param {int} 	ID que vai ser apagado da lista de utilizadores online
 */
void unsetOnlineUser(int id)
{
	memset(&OnlineUsers[id], 0, sizeof(OnlineUser));
}


/**
 * Carrega os utilizadores a partir do ficheiro users.dat
 * @return {int} 	Status:	
 *         		     1 - Carregou o ficheiro com sucesso
 *         		     0 - Falhou a carregar o ficheiro
 */
int LoadUsers()
{
	FILE *usersFile = fopen("users.dat", "r");
	char username[MAX_USER_LENGTH];
	char password[MAX_USER_LENGTH];
	#ifdef __DEBUG__
	int j = 1; //O primeiro user será sempre o admin:admin
	#else
	int j = 0;
	#endif
	if (usersFile == NULL)
	{
		printf("A criar ficheiro users.dat... ");
		usersFile = fopen("users.dat", "w");

        if (usersFile == NULL)
        	printf("[%sFail%s]\n", KRED, KNRM);
        else
        	printf("[%sDone%s]\n", KGRN, KNRM);

        //Podemos retornar aqui porque o ficheiro que criamos está vazio
        return usersFile != NULL;		
	}

	while(!feof(usersFile))
	{
		fscanf(usersFile, "%s %s", username, password);
		if (UserExists(username) >= 0 || strlen(username) == 0
			|| strlen(password) == 0) 
		{
			/**
			 * 1 - Caso o ficheiro seja modificado manualmente 
			 * e exista o mesmo utilizador outra vez
			 * ou
			 * 2 - a leitra nao tenha sido válida
			 */
			continue;
		}
		strcpy(Users[j].name, username);
		strcpy(Users[j].password, password);
		//"reset" aos arrays...	
		username[0] = '\0';	
		password[0] = '\0';	
		++j;
	}

	return 1;
}


/**
 * Adiciona um utilizador ao ficheiro/"base de dados" que contem os utilizadores registados
 * @param  {char*}  	username 	Utilizador a adicionar
 * @return {int}	   Status:	
 *         		    	1 - Carregou o ficheiro com sucesso
 *         		     	0 - Falhou a carregar o ficheiro
 */
int AddUser(char* username)
{
	int i;
	for (i = 0; username[i] != 0; ++i)
	{
		username[i] = tolower(username[i]);
	}
	if (UserExists(username) >= 0)
	{
		printf("O utilizador %s já existe\n", username);
		return 0;
	}
	char *password;
	char *password_conf;
	password = getpass("Password? ");
	password_conf = getpass("Repita a password? ");
	if (strcmp(password, password_conf) != 0)
	{
		fprintf(stderr, "As passwords não coincidem\n");
		return 0;
	}

	FILE *usersFile = fopen("users.dat", "a");
	if (usersFile == NULL)
	{
		printf("Falha ao abrir/criar users.dat. Verifique as permissões\n");
		return 0;
	}

	fprintf(usersFile, "%s %s\n", username, password);
	printf("Utilizador %s adicionado.\n", username);
	return 1;
}