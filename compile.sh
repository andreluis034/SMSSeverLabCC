#!/bin/sh

if [ "$1" = "debug" ]; then
	gcc -o SMSServer main.c messagehelper.c socketwrapper.c user.c -lpthread -Wall -g -O0 -D __DEBUG__
else
	gcc -o SMSServer main.c messagehelper.c socketwrapper.c user.c -lpthread
fi
