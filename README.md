André Brandão		up201503320@fc.up.pt 	201503320   
Rúben Carvalho		up201508742@fc.up.pt   	201508742
  

# SMSServer

  

SMSServer é um simples servidor criado para envio e receção de mensagens atravês da internet.   
Pode ser utilizado com este cliente: https://gitlab.com/andreluis034/SMSClient
 - Envia e recebe mensagens de tamanho arbritário(desde que tenha memórias suficiente)    
 - Anti message spoofing
 - Fecha todos os sockets corretamente caso o programa seja interrompido(CTRL + C)
 - Logout ao user caso tenha sessão iniciada e seja inciada uma nova noutro local
 - Mensagens coloridas! :D
   

### Compilação
Para compilar o servidor deve executar um dos seguintes comandos:
##### Release
   
```sh
gcc -o SMSServer main.c messagehelper.c socketwrapper.c user.c -lpthread
```   
    
    
##### Debug
```sh
gcc -o SMSServer main.c messagehelper.c socketwrapper.c user.c -lpthread -g -O0 -D __DEBUG__	
```
  
    
     
### Utilização

	▌ Executar o servidor:

		Deve executar o servidor onde pretende hospedar o seu SMSServer.

			Para executar o servidor deve, cajo nao o tenha feito, compilar o SMSServer e executar o seguinte comando:
			
			./SMSServer

	
		Se pretender ver todas as mensagens enviadas pelos utilizadores pode utilizar o argumento -NSA na execução do servidor:
		Exemplo:

		./SMSServer -NSA


		A porta pré-definida é a porta 1337.
		Caso pretenda que o servidor aceite ligações noutra porta use o argumento -p <porta>:
		Exemplo:

		./SMSServer -p 1338



	▌ Adicionar novos utilizadores:
	
		Para adicionar novos utilizadores use o argumento adduser <username>. Por exemplo:
	
		./SMSServer adduser Manuel

		Os nomes de utilizadores não são sensíveis à capitalização.

		O servidor quando compilado em modo DEBUG, ativa o utilizador admin:admin

