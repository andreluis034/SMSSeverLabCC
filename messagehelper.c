#include "messagehelper.h"

/**
 * Dado um apontador para uma mensagem(Tipo Message) retorna o seu tamanho
 * Isto é necessário pois as mensagens são alocadas dinamicamente
 * @param  {Message*}		Apontador para a mensagem
 * @return {int} 			Tamanho da mensagem
 */
int actualMessageSize(Message* message)
{
	//+1 é do null Byte no final da string
	//MAX_USER_LENGTH * 2 é o tamano do sender + user
	return sizeof(int) + sizeof(message->Type) + MAX_USER_LENGTH * 2 + strlen(message->Content) + 1;
}

/**
 * Cria uma mensagem a partir dos argumentos dados
 * @param  {MessageType}        Tipo da mensagem(definido em messagehelper.h)
 * @param  {Char*}				De quem vem a mensagem
 * @param  {Char*} 				Destino da mensagem
 * @param  {Char*}     			Mensagem
 * @return {Message*} 			Apontador para a mensagem
 */
Message* createMessage(MessageType mType, char* source, char* destination, char* content)
{
	int size = sizeof(int) + sizeof(mType) + MAX_USER_LENGTH * 2 + strlen(content)+1;
	Message* message = malloc(size); //malloc aloca no heap da aplicacao
	memset(message, 0, size);
	memcpy(&message->Length, &size, sizeof(int));
	memcpy(&message->Type, &mType, sizeof(mType));
	strcpy(message->Sender, source);
	strcpy(message->Destination, destination);
	strcpy(message->Content, content);

	return message;
}

/**
 * Envia a mensagem dada no argumento ao destino(message->Destination)
 * @param  {Message*}  			Apontador para a mensagem que vai ser enviada
 * @param  {char*}				Pessoa que enviou a mensagem, necessário para que a pessoa 
 *                    			que envia a mensagem não possa fazer spoof e enviar mensagens
 *                    			em nome de outra pessoa
 * @return {int} status 		0 - A mensagem é inválida
 *               				1 - Sucedido
 *               				2 - O utilizador nao se encontra online
 */
int sendMessage(Message* message, char* sender)
{
	int destID;
	int status;
	char destino[200];
	Message* sndMessage;

	//Mensagem inválida/irrelevante
	if ((int)strlen(message->Destination) == 0 || (int)strlen(message->Content) == 0)
		return 0;

	
	//Os utilizadores nao sao case-sensitive
	int i;
	for (i = 0; message->Destination[i] != 0; ++i)
	{
		message->Destination[i] = tolower(message->Destination[i]);
	}
	destID = getOnlineUserIdFromName(message->Destination);

	if (destID < 0){
		destID = getOnlineUserIdFromName(sender);
		sprintf(destino,"%s%s nao se encontra online!%s",KRED,message->Destination,KNRM);
		sndMessage = createMessage(INFO,"\0","\0",destino);
		status = writeSocket(OnlineUsers[destID].socket, sndMessage, sndMessage->Length);
		free(sndMessage);
		return 2;
	}
		

	//Criar a mensagem para enviar
	sndMessage = createMessage(RECVMSG, sender, 
		message->Destination, message->Content);

	status = writeSocket(OnlineUsers[destID].socket, sndMessage, sndMessage->Length);
	if (status >= 0)
	{
		printf("-> %s enviou uma mensagem a %s\n", 
			sndMessage->Sender, sndMessage->Destination);
		if (NSAMODE == 1)
		{
			printf("-> %s\n", message->Content);
		}
	}
	free(sndMessage);
	return 1;
}

/**
 * Imprive a mensagem no stdout, principalmente para DEBUG
 * @param {Message*} 			Mensagem a imprimir no stdout
 */
void printMessage(Message* message)
{
	printf("Mensagem recebida:\nLength:%d\nType:%d\nSender:%s\nDestination:%s\nText:%s\n\n", 
		message->Length, message->Type, message->Sender, 
		message->Destination, message->Content);
}
